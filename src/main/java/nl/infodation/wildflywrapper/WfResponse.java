package nl.infodation.wildflywrapper;

public interface WfResponse {
	
	
	/**
	 * Message from the local client (this package) about executing the givencommand. 
	 * Mostly <strong>null</strong>. 
	 */
	public abstract String getLocalMsg();
	
	/**
	 * give response from the wildfly server. 
	 * <strong>null</strong>  when there was no response.
	 */
	public abstract String getServerMsg();
	
	/**
	 * sometimes wildfly server need to do a reload to make the executed command to have an actual effect.
	 * @return true when a reload is needed to effect the command
	 */
	public abstract boolean isReloadNeeded();


}
