/**
 * 
 */
package nl.infodation.wildflywrapper.commands;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.logging.Level;
import java.util.logging.Logger;

import nl.infodation.wildflywrapper.WfServer;

/**
 * @author hugo
 *
 */
public abstract class AbstractCommand implements WfCommand{
	
	private static final Logger LOG = Logger.getLogger(AbstractCommand.class.getName());
	
	protected Set<String> settings = new CopyOnWriteArraySet<>();
	protected Map<String, String> params = new ConcurrentHashMap<>();

	@Override
	public WfCommand set(String paramName, String paramValue) throws NullPointerException {
		if (paramName == null) {
			throw new NullPointerException("parameter name must be given to set value " + paramValue);
		}
		String oldValue = params.put(paramName, paramValue);
		if (LOG.isLoggable(Level.FINE)) {
			LOG.fine("Parameter \""+paramName+"\" set from \"" + oldValue + "\" to \"" + paramValue + "\"");
		}
		return this;
	}

	@Override
	public WfCommand set(String setting) throws NullPointerException {
		if (setting == null) {
			throw new NullPointerException("Setting name cannot be null");
		}
		if (!settings.add(setting)) {
			LOG.info("Ignoring setting \"" + setting + "\" twice");
		}
		return this;
	}

	/**
	 * @see nl.infodation.wildflywrapper.commands.WfCommand#remove(java.lang.String)
	 */
	@Override
	public WfCommand remove(String setting) throws NullPointerException {
		if (setting == null) {
			throw new NullPointerException("Setting name to remove cannot be null");
		}
		if (!settings.remove(setting)) {
			LOG.info("Ignoring removal of setting \"" + setting + "\" becuase it was not set in the first place");
		}
		return this;
	}
	
	/**
	 * This method is actually used in the execute method of the WfServer. It must result in a valid CLI command line.
	 * @see java.lang.Object#toString()
	 * @see WfServer#execute(WfCommand)
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getType().toString());
		for (String setting: settings) {
			sb.append(' ');
			sb.append(setting);
		}
		for (String key: params.keySet()) {
			sb.append(" --");
			sb.append(key);
			sb.append("=\"");
			sb.append(params.get(key));
			sb.append('\"');
		}
		return sb.toString();
	}	
}
