/**
 * 
 */
package nl.infodation.wildflywrapper.commands;

/**
 * subsystems in wildfly server
 * @author hugo
 *
 */
public enum WfSubSystem {

	jaxrs("jaxrs"),
	jpa("jpa"),
	ee("ee"),
	transactions("transactions"),
	remoting("remoting"),
	jmx("jmx"),
	security("security"),
	weld("weld"),
	pojo("pojo"),
	infinispan("infinispan"),
	jca("jca"),
	requestController("request-controller"),
	undertow("undertow"),
	beanValidation("bean-validation"),
	securityManager("security-manager"),
	datasources("datasources"),
	logging("logging"),
	naming("naming"),
	webservices("webservices"),
	jsf("jsf"),
	jdr("jdr"),
	batchJberet("batch-jberet"),
	deploymentScanner("deployment-scanner"),
	ejb3("ejb3"),
	io("io"),
	mail("mail"),
	sar("sar"),
	resourceAdapters("resource-adapters")
	;
	
	private final String cliName;
	
	
	private WfSubSystem(String name) {
		cliName = name;
	}
	
	public String getCliName() {
		return cliName;
	}

}