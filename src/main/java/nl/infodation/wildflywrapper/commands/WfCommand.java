/**
 * 
 */
package nl.infodation.wildflywrapper.commands;

/**
 * A command to execute on the wildfly server
 * 
 * @author hugo
 *
 */
public interface WfCommand {

	public abstract String toString();

	/**
	 * TODO The value will be escaped if necessary? 
	 * @param paramName
	 *            not null, name of the parameter
	 * @param paramValue
	 *            value for the parameter. 
	 *            <p>a <code>null</code> value will actually remove the parameter.</P>
	 * @throws NullPointerException
	 *             when paramName is null
	 */
	public abstract WfCommand set(String paramName, String paramValue) throws NullPointerException;

	/**
	 * 
	 * @param setting
	 *            not null, name of the setting
	 * @throws NullPointerException
	 *             when setting is null
	 */
	public abstract WfCommand set(String setting) throws NullPointerException;

	/**
	 * 
	 * @param setting
	 *            not null, name of the setting
	 * @throws NullPointerException
	 *             when setting is null
	 */
	public abstract WfCommand remove(String setting) throws NullPointerException;

	/**
	 * Get the main type of the command. for example: data-source
	 * 
	 * @return never null
	 */
	public abstract WFCommandType getType();
}
