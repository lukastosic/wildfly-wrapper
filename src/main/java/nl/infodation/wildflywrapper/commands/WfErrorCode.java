/**
 * 
 */
package nl.infodation.wildflywrapper.commands;

/**
 * @author hugo
 *
 */
public enum WfErrorCode {
	

	/**
	 * Service is already registered
	 */
	WFLYCTL0158("Service is already registered")
	/**
	 * Resource not found
	 */
	, WFLYCTL0216("Resource not found")
	
	/**
	 * Composite operation failed and was rolled back
	 */
	, WFLYCTL0062("Composite operation failed and was rolled back")
	
	/**
	 * Resource does not exist; a resource cannot be created until all ancestor resources have been added
	 */
	, WFLYCTL0175("Resource does not exist; a resource cannot be created until all ancestor resources have been added");
	;
	
	private final String msg;
	
	private WfErrorCode(String message) {
		msg = message;
	}

	/**
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return super.toString() + "(" + msg + ")";
	}
	
}
