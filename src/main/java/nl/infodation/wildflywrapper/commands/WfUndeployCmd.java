package nl.infodation.wildflywrapper.commands;

public class WfUndeployCmd extends AbstractCommand {

	@Override
	public WFCommandType getType() {
		return WFCommandType.undeploy;
	}

	/**
	 * @param appArchive
	 *            location where the war/ear/jar/sar file can be read from the
	 *            local code.
	 * @param force
	 *            if the deployment with the specified name already exists, by
	 *            default, deploy will be aborted and the corresponding message
	 *            will printed. Switch --force (or -f) will force the
	 *            replacement of the existing deployment with the one specified
	 *            in the command arguments.
	 * @return deployment command for given archive
	 */
	public static WfCommand undeploy(String name) {
		return new WfUndeployCmd().set(name);
	}
}
