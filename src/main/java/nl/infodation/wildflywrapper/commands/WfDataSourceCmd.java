/**
 * 
 */
package nl.infodation.wildflywrapper.commands;

import java.net.MalformedURLException;

import nl.infodation.wildflywrapper.WfServer;

/**
 * Creates commands to be executed on the wildfly server. 
 * @author hugo
 *
 */
public class WfDataSourceCmd extends AbstractCommand {
	
	public static final String DEFAULT_DRIVER = "mysql";
	public static final String DEFAULT_JNDI_PREFIX = "java:/datasources/";
	
	@Override
	public WFCommandType getType() {
		return WFCommandType.data_source;
	}

	/*
			String command = "data-source add --jndi-name=" + DB_JNDI_NAME 
			+ " --name=" + DB_CONNECTION_NAME
			+ " --connection-url=" + dbUrl 
			+ " --driver-name=" + DB_DRIVER_NAME 
			+ " --user-name=" + DB_USERNAME 
			+ " --password=" + DB_PASSWORD
			+ " --exception-sorter-class-name=org.jboss.jca.adapters.jdbc.extensions.mysql.MySQLExceptionSorter" 
	;
	if (validating) {
		command = command 
			+ " --check-valid-connection-sql=SELECT\\ 1"  
			+ " --validate-on-match=true"
			+ " --track-statements=true"
			+ " --background-validation=true"
			;
	 */
	/**
	 * Command to Create a new datasource
	 * @param dsName name for the datasource
	 * @param jndi the jndi name for the datasource
	 * @param dbUrl the JDBC url to the database used for the datasource
	 * @param driver name of the driver defined in the wildfly server. 
	 * Must already exist before this command can be executed  successfully.
	 * @param username username for the database
	 * @param password password for the database
	 * @return the command. 
	 * @see WfServer#execute(WfCommand)
	 */
	public static WfCommand addDs(String dsName, String jndi, String dbUrl, String driver, String username, String password) {
		WfDataSourceCmd cmd = new WfDataSourceCmd();
		cmd.set("add")
		.set("jndi-name", jndi)
		.set("name", dsName)
		.set("connection-url",dbUrl)
		.set("driver-name", driver)
		.set("check-valid-connection-sql", "SELECT 1")
		.set("validate-on-match","true")
		.set("background-validation","true")
		;
		if (DEFAULT_DRIVER.equals(driver)) {
			cmd.set("exception-sorter-class-name", "org.jboss.jca.adapters.jdbc.extensions.mysql.MySQLExceptionSorter");
		}
		if (username != null) {
			cmd.set("user-name", username);
		}
		if (password != null) {
			cmd.set("password", password);
		}
		return cmd;
	}

	public static WfCommand addDs(String dsName, String jndi, String dbUrl, String username, String password) {
		return addDs(dsName, jndi, dbUrl, DEFAULT_DRIVER, username, password);
	}
	
	public static WfCommand addDs(String dbUrl, String username, String password) throws MalformedURLException {
		String dbName = getDbName(dbUrl);
		String driver = getDbType(dbUrl);
		String jndi = DEFAULT_JNDI_PREFIX + dbName;
		return addDs(dbName, jndi, dbUrl, driver, username, password);
	}
	
	public static WfCommand removeDs(String dsName) {
		return 
			new WfDataSourceCmd()
			.set("remove")
			.set("name", dsName)
		;
	}
	
	public static WfCommand testDs(String dsName) {
		return new WfDataSourceCmd()
		 .set("test-connection-in-pool")
			.set("name", dsName)
		;
			
	}
	
	/**
	 * 
	 * @param dbUrl not null
	 * @return the subtype after jdbc
	 * @throws MalformedURLException when given URL cannot be parsed
	 */
	private static String getDbType(String dbUrl) throws MalformedURLException {
		// ex. : jdbc:mysql://localhost:3306/efap.blobstore_0
		int startType = dbUrl.indexOf(':');
		if (startType < 0) {
			throw new MalformedURLException("Unable to parse database URL \"" + dbUrl);
		}
		startType++;
		int endType = dbUrl.indexOf("://",startType);
		if (endType < 0) {
			throw new MalformedURLException("Unable to parse database URL \"" + dbUrl);
		}
		return dbUrl.substring(startType, endType);	
	}
	
	private static String getDbName(String dbUrl) throws MalformedURLException {
		int startName = dbUrl.lastIndexOf('/');
		if (startName < 0) {
			throw new MalformedURLException("Unable to parse database URL \"" + dbUrl);
		}
		startName++;
		int endName = dbUrl.indexOf( '?', startName);
		if (endName < 0) { // no parameters added to URL tehrefore database name is last part of the string
			endName = dbUrl.length();
		}
		if (endName - startName < 1) {
			throw new MalformedURLException("Unable to parse database URL \"" + dbUrl);
		}
		return dbUrl.substring(startName, endName);
	}

}
