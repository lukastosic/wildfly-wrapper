/**
 * 
 */
package nl.infodation.wildflywrapper.commands;

/**
 * read from wildfly server commands.
 * @author hugo
 *
 */
public class WfReadCmd extends AbstractCommand {
	
	/**
	 * @see nl.infodation.wildflywrapper.commands.WfCommand#getType()
	 */
	@Override
	public WFCommandType getType() {
		return WFCommandType.ls;
	}
	
	/**
	 * Convenience method
	 * @return command to get list of all nonXA datasource names
	 */
	public static WfCommand listDs() {
		return list(WfSubSystem.datasources);
	}
	
	/**
	 * 
	 * @param subSystem not null
	 * @return
	 */
	public static WfCommand list(WfSubSystem subSystem) {
		return new WfReadCmd().set("/subsystem="+subSystem.getCliName()+"/:read-resource")
				;
	}
	
	public static WfCommand existsDs(String nameDs) {
		return new WfReadCmd().set("/subsystem=datasources/data-source="+nameDs+":read-attribute(name=enabled)");
	}
	
	public static WfCommand existsDriver(String nameDriver) {
		return new WfReadCmd().set("/subsystem=datasources/jdbc-driver="+nameDriver+":read-attribute(name=driver-name)");
	}
	
//	public static WfCommand existsJndi(String jndi) {
//		return new WfReadCmd().set("/subsystem=datasources/jdbc-driver="+nameDriver+":read-attribute(name=driver-name)");
//	}
		

	/**
	 * @see nl.infodation.wildflywrapper.commands.AbstractCommand#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (String setting: settings) {
			sb.append(' ');
			sb.append(setting);
		}
		for (String key: params.keySet()) {
			sb.append(" --");
			sb.append(key);
			sb.append("=\"");
			sb.append(params.get(key));
			sb.append('\"');
		}
		return sb.toString();
	}
}
