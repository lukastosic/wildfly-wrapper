package nl.infodation.wildflywrapper.commands;

import java.io.File;
import java.net.URL;

public class WfDeployCmd extends AbstractCommand {

	@Override
	public WFCommandType getType() {
		return WFCommandType.deploy;
	}
	
	/**
	 * @param appArchive location where the war/ear/jar/sar file can be read from the local code. 
	 * @param force if the deployment with the specified name already exists,
                     by default, deploy will be aborted and the corresponding
                     message will printed. Switch --force (or -f) will force the
                     replacement of the existing deployment with the one
                     specified in the command arguments.
	 * @return deployment command for given archive
	 */
	public static WfCommand deploy(final File appArchive, boolean force) {
		WfCommand cmd = new WfDeployCmd();
		if (force) {
			cmd.set("--force");
		}
		return cmd.set('\"' + appArchive.getPath() + '\"');
	}

	/**
	 * @param appArchive location where the war/ear/jar/sar file can be downloaded by the wildfly server. 
	 * @param force if the deployment with the specified name already exists,
                     by default, deploy will be aborted and the corresponding
                     message will printed. Switch --force (or -f) will force the
                     replacement of the existing deployment with the one
                     specified in the command arguments.
	 * @return deployment command for given archive
	 */
	public static WfCommand deploy(final URL appArchive, boolean force) {
		WfCommand cmd = new WfDeployCmd();
		if (force) {
			cmd.set("--force");
		}
		return cmd.set("url", appArchive.toExternalForm());
	}
	
	public static WfCommand list() {
		return new WfDeployCmd();
	}
}
