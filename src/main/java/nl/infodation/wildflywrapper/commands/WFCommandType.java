/**
 * 
 */
package nl.infodation.wildflywrapper.commands;

/**
 * All possible types of commands on the CLI
 * @author hugo
 *
 */
public enum WFCommandType {
	
	batch("batch"),
	data_source("data-source"),
	help("help"),
	patch("patch"),
	run_batch("run-batch"),
	version("version"),
	cd("cd"),
	deploy("deploy"),
	history("history"),
	pwd("pwd"),
	set("set"),
	xa_data_source("xa-data-source"),
	clear("clear"),
	deployment_info("deployment-info"),
	IF("if"),
	quit("quit"),
	shutdown("shutdown"),
	command("command"),
	deployment_overlay("deployment-overlay"),
	jdbc_driver_info("jdbc-driver-info"),
	read_attribute("read-attribute"),
	TRY("try"),
	connect("connect"),
	echo("echo"),
	ls("ls"),
	read_operation("read-operation"),
	undeploy("undeploy"),
	connection_info("connection-info"),
	echo_dmr("echo-dmr"),
	module("module"),
	reload("reload"),
	unset("unset");
	
	private final String cmd;
			
	private WFCommandType(String command) {
		cmd = command;
	}

	/**
	 * gives the string used in the CLI for the given command type.
	 * for example: type data_source will result in data-source
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return cmd;
	}
}
