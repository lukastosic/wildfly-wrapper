/**
 * 
 */
package nl.infodation.wildflywrapper.util;

import java.io.Serializable;

import nl.infodation.wildflywrapper.WfResponse;

/**
 * A response from a command
 * @author hugo
 *
 */
public class WfResponseImpl implements Serializable, WfResponse {

	private static final long serialVersionUID = 1L;
	
	private final String localMsg;
	private final String serverMsg;
	private final boolean reloadNeeded;
	
	/**
	 * 
	 * @param localMsg message from the local part of the code (this wrapper).  
	 * @param serverMsg message from the wildfly server. 
	 */
	public WfResponseImpl(String localMsg, String serverMsg, boolean reload) {
		super();
		this.localMsg = localMsg;
		this.serverMsg = serverMsg;
		this.reloadNeeded = reload;
	}

	/**
	 * @see nl.infodation.wildflywrapper.WfResponse#getLocalMsg()
	 */
	public String getLocalMsg() {
		return localMsg;
	}

	/**
	 * @see nl.infodation.wildflywrapper.WfResponse#getServerMsg()
	 */
	public String getServerMsg() {
		return serverMsg;
	}

	/**
	 * @return the reloadNeeded
	 * @see nl.infodation.wildflywrapper.WfResponse#isReloadNeeded()
	 */
	public boolean isReloadNeeded() {
		return reloadNeeded;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("WfResponseImpl [localMsg=");
		builder.append(localMsg);
		builder.append(", serverMsg=");
		builder.append(serverMsg);
		builder.append(", reloadNeeded=");
		builder.append(reloadNeeded);
		builder.append("]");
		return builder.toString();
	}

}
