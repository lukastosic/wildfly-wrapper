/**
 * 
 */
package nl.infodation.wildflywrapper.util;

import java.util.NoSuchElementException;
import java.util.logging.Logger;

import org.jboss.as.cli.scriptsupport.CLI.Result;
import org.jboss.dmr.ModelNode;

import nl.infodation.wildflywrapper.CommandFailedException;
import nl.infodation.wildflywrapper.WfResponse;

/**
 * Creates WfResponse objects based on response from WildFly
 * @author hugo
 *
 */
public class WfResponseFactory {
	
	private static final Logger LOG = Logger.getLogger(WfResponseFactory.class.getName());
	
	/**
	 * {
	 *   "outcome" => "success",
	 *   "result" => "127.0.0.1"
	 * }
	 */
	private static final String NAME_RESULT = "result";
	
	/**
	 * {"outcome" => "success";
	 */
	private static final String NAME_OUTCOME = "outcome";
	
	/**
	 * {"outcome" => "success";
	 */
	private static final String VALUE_OUTCOME_SUCCESS = "success"; 
	
	/**
	 * "process-state" => "reload-required"
	 */
	private static final String NAME_STATUS = "process-state";
	
	/**
	 * "process-state" => "reload-required"
	 */
	private static final String VALUE_STATUS_RELOAD = "reload-required";

	private static final String NAME_FAILURE = "failure-description";

	private static final String NAME_RESP_HEADERS = "response-headers";

	/**
	 * 
	 * @param node not null. The response node from the wildfly response
	 * @return parsed response deom node
	 * @throws CommandFailedException when the command was not successful
	 * @see #throwEx(ModelNode) 
	 */
	public static WfResponse create(final Result result) throws CommandFailedException {
		if (result == null) {
			throw new CommandFailedException("Response is null");
		}
		ModelNode node = result.getResponse();
		if (node == null) {
			throw new CommandFailedException("Value of response is empty for command: " + result.getCliCommand());
		}
		node.protect();
		if (!result.isSuccess()) {
			throwEx(node, result.getCliCommand());
		}
		if (!isSet(node, NAME_OUTCOME, VALUE_OUTCOME_SUCCESS)) {
			throwEx(node, result.getCliCommand());
		}
		boolean reload = false;
		if (node.has(NAME_RESP_HEADERS)) {
			reload = isSet(node.get(NAME_RESP_HEADERS), NAME_STATUS, VALUE_STATUS_RELOAD);
		}
		
		ModelNode mn = node;
		if (node.has(NAME_RESULT)) {
			mn = node.get(NAME_RESULT);
		}
		return new WfResponseImpl(result.getRequest().asString(), mn.asString(), reload); 
	}
	
	/**
	 * 
	 * @param node not null. the response node of the result from command execution 
	 * @throws CommandFailedException always
	 * @see #create(ModelNode, boolean)
	 */
	private static void throwEx(ModelNode node, String cliCmd) throws CommandFailedException {
		ModelNode mn = node;
		if (node.has(NAME_FAILURE)) {
			mn = node .get(NAME_FAILURE);
		}
		if (cliCmd == null) { 
			throw new CommandFailedException(mn.asString());
		}
		throw new CommandFailedException(mn.asString() + ", cmd=" + cliCmd);
	}
	
	private static boolean isSet(final ModelNode node,String name, String expectedValue) {
		if (node == null) {
			LOG.fine("Cannot find value \"" + expectedValue + "\" for node \"" + name + "\" because response node is null");
			return false;
		}
		if (name == null) {
			throw new NullPointerException("name of a child node cannot be null. cannoot look for expected value " + expectedValue);
		}
		if (expectedValue == null) {
			throw new NullPointerException("expectedValue of child node \""+name+"\" cannot be null");
		}
		try {
			ModelNode child = node.require(name);
			return child.asString().equals(expectedValue);
		} catch (NoSuchElementException e) {
			LOG.fine("Cannot find value \"" + expectedValue + "\" for node \"" + name + "\" because parent node does not have this child node");
			return false;
		}
	}
}
