/**
 * 
 */
package nl.infodation.wildflywrapper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Executing a command to the wildfly server failed.
 * 
 * @author hugo
 *
 */
public class CommandFailedException extends Exception {

	private static final long serialVersionUID = 1L;
	private static final Pattern PATT_WF_ERROR_CODE = Pattern.compile("WFLYCTL(\\d)+");

	private final boolean retryAble;
	private final String errorCode;

	public CommandFailedException(String message) {
		super(message);
		retryAble = false;
		errorCode = findErrorCode(message);
	}

	public CommandFailedException(String message, boolean canRetry) {
		super(message);
		this.retryAble = canRetry;
		errorCode = findErrorCode(message);
	}

	public CommandFailedException(String message, boolean canRetry, Exception e) {
		super(message, e);
		this.retryAble = canRetry;
		errorCode = findErrorCode(message);
	}

	/**
	 * Server was doing something so it cannot execute the command. But as soon as the server finishes, the same command can be tried again. 
	 * Used when server is rebooting or reloading
	 * @return the retryAble
	 */
	public boolean isRetryAble() {
		return retryAble;
	}

	private String findErrorCode(String message) {
		if (message == null) {
			return null;
		}
		Matcher matcher = PATT_WF_ERROR_CODE.matcher(message);
		if (matcher.find()) {
			String errorCode = matcher.group(0);
			if (errorCode != null && errorCode.length() > 1) {
				return errorCode;
			}
		}
		return null;
	}

	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CommandFailedException [message=");
		builder.append(getLocalizedMessage());
		builder.append(", retryAble=");
		builder.append(retryAble);
		builder.append(", errorCode=");
		builder.append(errorCode);
		builder.append("]");
		return builder.toString();
	}
}
