/**
 * 
 */
package nl.infodation.wildflywrapper;

import java.util.Collection;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jboss.as.cli.CommandFormatException;
import org.jboss.as.cli.scriptsupport.CLI;
import org.jboss.as.cli.scriptsupport.CLI.Result;

import nl.infodation.wildflywrapper.commands.WfCommand;
import nl.infodation.wildflywrapper.commands.WfErrorCode;
import nl.infodation.wildflywrapper.commands.WfReadCmd;
import nl.infodation.wildflywrapper.commands.WfSubSystem;
import nl.infodation.wildflywrapper.data.WfConfig;
import nl.infodation.wildflywrapper.util.WfResponseFactory;

/**
 * A proxy for executing commands on the CLI of a wildfly server
 * @author hugo
 *
 */
public class WfServer {
	
	private static final Logger LOG = Logger.getLogger(WfServer.class.getName());
	private static final String LIST_START = " => {";
	private final WfConfig config;
	private final CLI cli;

	
	
	
	/**
	 * 
	 * @param password the password for the administrative console of the wildfly config
	 * @return a new wrapper instance
	 */
	public WfServer(String password) {
		config = new WfConfig(password);
		cli = CLI.newInstance();
	}
	
	/**
	 * Sets the server to which to connect. Can be IP number or dns name. 
	 * default value is  'localhost'
	 * @param server not null
	 * @return wrapper with given server
	 * @throws NullPointerException when given server is null  
	 */
	public WfServer setServer(String server) {
		if (server == null) {
			throw new NullPointerException("Server for wrapper cannot be null");
		}
		this.config.setServer(server);
		return this;
	}
	
	public WfServer setPort(int portNr) {
		config.setPort(portNr);
		return this;
	}
	
	/**
	 * Sets the username.
	 * default value is 'admin'
	 * @param userName name of the user to log in into the admin console of the server.
	 * @return wrapper with given user
	 */
	public WfServer setUser(String userName) {
		config.setUsername(userName);
		return this;
	}
	
	/**
	 * 
	 * sets the protocol used to connect to the wildfly server. 
	 * default is "http-remoting"
	 * @param proto not null. 
	 * @return wrapper that will use the given protocol.
	 * @throws NullPointerException when given protocol is null 
	 */
	public WfServer setProtocol(String proto) {
		if (proto== null) {
			throw new NullPointerException("Protocol is not allowed to be null");
		}
		config.setProtocol(proto);
		return this;
	}
	
	/**
	 * 
	 * @return list of all non-xa datasources in the wildfly server. List can be empty, but never null.
	 * @throws CommandFailedException when list command to wildlfy server fails.
	 */
	public Collection<String> list(WfSubSystem subsystem) throws CommandFailedException {
		WfCommand listCmd = WfReadCmd.list(subsystem);
		WfResponse result = execute(listCmd);
		String s = result.getServerMsg();
		 Collection<String> names= new LinkedList<>();
			int start = s.indexOf(LIST_START);
		if (start < 0) {
			if (LOG.isLoggable(Level.FINE)) {
				LOG.fine("No "+subsystem+" names found in " + s);
			}
			return names;
		}
		start += LIST_START.length();
		int end = s.indexOf('}', start);
		Matcher m = Pattern.compile("\"(.*?)\" => undefined").matcher(s.substring(start, end));
		while (m.find()) {
			names.add(m.group(1));
		}
		return names;
	}
	
	/**
	 * Check to see if specific datasource with given name exists.
	 * @param nameDs not null. case  sensitive
	 * @return response with serverMsg=true when datasource was found <strong>and enabled</strong>. 
	 * @throws CommandFailedException when datasource was not found. has errode WFLYCTL0216 when Ds was not found.
	 * @see WfErrorCode#WFLYCTL0216
	 */
	public WfResponse existsDs(String nameDs) throws CommandFailedException {
		/*
		 	{
    			"outcome" => "success",
    			"result" => true,
    			"response-headers" => {"process-state" => "reload-required"}
			}
		 */
		return execute(WfReadCmd.existsDs(nameDs));
	}

	public WfResponse existsDriver(String nameDriver) throws CommandFailedException {
		/*
		 {
    		"outcome" => "success",
    		"result" => "h2",
    		"response-headers" => {"process-state" => "reload-required"}
		}
		 */
		return execute(WfReadCmd.existsDriver(nameDriver));
	}
	
//	public WfResponse existsJndi(String jndi) throws CommandFailedException {
//		return execute(WfReadCmd.existsJndi(jndi));
//	}
//
	public WfResponse execute(WfCommand cmd) throws CommandFailedException {
		synchronized (cli) {
		try { 
			if (LOG.isLoggable(Level.FINE)) {
				cli.getCommandContext().setSilent(false);
				LOG.fine("Executing:\n" + cli.getCommandContext().buildRequest(cmd.toString()));
			}
			Result result = null;
			try {
				try { 
					result = cli.cmd(cmd.toString());
				} catch (IllegalStateException e) { // not connected yet
					LOG.info("Opening connection to " + config);
					try {
						cli.connect(config.getProtocol(), config.getServer(), config.getPort(), config.getUsername(), config.getPwd().toCharArray());
					} catch (IllegalStateException e1) { // Unable to connect to controller.
						throw new CommandFailedException(e1.getMessage(), false, e1);
					}
					result = cli.cmd(cmd.toString());
				}
			} catch (IllegalArgumentException e) {
				throw new CommandFailedException(e.getMessage(), false, e);
			}
			return WfResponseFactory.create(result);
		} catch (CommandFormatException e) {
			throw new CommandFailedException("Not a valid command: " + cmd.toString(), false, e);
		}
		}
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return config.hashCode();
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WfConfig other = ((WfServer)obj).config;
		return config.equals(other);
	}

	/**
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		if (cli != null)  {
			synchronized (cli) {
				cli.disconnect();
			}
		}
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("WfServer [config=");
		builder.append(config);
		builder.append("]");
		return builder.toString();
	}
}
