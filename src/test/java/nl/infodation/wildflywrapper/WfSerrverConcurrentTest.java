/**
 * 
 */
package nl.infodation.wildflywrapper;

import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import nl.infodation.wildflywrapper.commands.WfSubSystem;

/**
 * Tests the usage of the wildflywrapper in multithreaded usage. 
 * You have to have a wildfly running and configure the connection in the setUp() method
 * @author hugo
 * @see #setUp()
 *
 */
public class WfSerrverConcurrentTest {
	
	private static final Logger LOG = Logger.getLogger(WfSerrverConcurrentTest.class.getName());
	private static final int SIZE  = 200;
	private WfServer wf = null;


	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		// TODO configure your wildlfy connection here.
		wf = new WfServer("");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		wf = null;
	}

	/**
	 * Test method for {@link nl.infodation.wildflywrapper.WfServer#list(nl.infodation.wildflywrapper.commands.WfSubSystem)}.
	 * @throws Exception 
	 */
	@Test
	public void testList() throws Exception {
		ScheduledExecutorService executor = Executors.newScheduledThreadPool(SIZE);
		for (int i = 0; i < SIZE*2; i++) {
			TestList command = new TestList(i, wf);
			executor.execute(command);
		}
		executor.shutdown(); // triggers execution of all commands
		if (!executor.awaitTermination((SIZE* 1) + 30 , TimeUnit.SECONDS)) {
			throw new Exception("Timeout waiting for tasks to finish");
		}		
	}

	/**
	 * Test method for {@link nl.infodation.wildflywrapper.WfServer#existsDs(java.lang.String)}.
	 * @throws Exception 
	 */
	@Test
	public void testExistsDs() throws Exception {
		ScheduledExecutorService executor = Executors.newScheduledThreadPool(SIZE);
		for (int i = 0; i < SIZE*2; i++) {
			TestExistsDs command = new TestExistsDs(i, wf);
			executor.execute(command);
		}
		executor.shutdown(); // triggers execution of all commands
		if (!executor.awaitTermination((SIZE* 1) + 30 , TimeUnit.SECONDS)) {
			throw new Exception("Timeout waiting for tasks to finish");
		}		
	}

	/**
	 * Test method for {@link nl.infodation.wildflywrapper.WfServer#existsDriver(java.lang.String)}.
	 * @throws Exception 
	 */
	@Test
	public void testExistsDriver() throws Exception {		
		ScheduledExecutorService executor = Executors.newScheduledThreadPool(SIZE);
		for (int i = 0; i < SIZE*2; i++) {
			TestExistsDriver command = new TestExistsDriver(i, wf);
			executor.execute(command);
		}
		executor.shutdown(); // triggers execution of all commands
		if (!executor.awaitTermination((SIZE * 1) + 30 , TimeUnit.SECONDS)) {
			throw new Exception("Timeout waiting for tasks to finish");
		}		
	}
	
	private class TestExistsDriver implements Runnable {
		
		final int id; 
		final WfServer wf;
		
		protected TestExistsDriver(int i, WfServer server) {
			id =i;
			wf = server;
		}

		@Override
		public void run() {
			LOG.fine("Task " + id + " start");
			WfResponse result;
			try {
				result = wf.existsDriver("h2");
				assertTrue("h2".equals(result.getServerMsg()));
			} catch (CommandFailedException e) {
				LOG.log(Level.WARNING,"Task " + id + " failed",  e);
				throw new RuntimeException("Task " + id + " failed",  e);
			}
			LOG.fine("Task " + id + " end");
		}	
	}

	private class TestExistsDs implements Runnable {
		
		final int id; 
		final WfServer wf;
		protected TestExistsDs(int i, WfServer server) {
			id =i;
			wf= server;
		}

		@Override
		public void run() {
			LOG.fine("Task " + id + " start");
			WfResponse result;
			try {
				result = wf.existsDs("ExampleDS");
				assertTrue("true".equals(result.getServerMsg()));
			} catch (CommandFailedException e) {
				LOG.log(Level.WARNING,"Task " + id + " failed",  e);
				throw new RuntimeException("Task " + id + " failed",  e);
			}
			LOG.fine("Task " + id + " end");
		}	
	}

	private class TestList implements Runnable {
		
		final int id;
		final WfServer wf;

		protected TestList(int i, WfServer server) {
			id =i;
			wf=server;
		}

		@Override
		public void run() {
			LOG.fine("Task " + id + " start");
			try {
				Collection<String> list = wf.list(WfSubSystem.datasources);
				assertTrue("List cannot be empty", list != null && list.size() > 0);
				assertTrue("Expected ExampleDS", list.contains("ExampleDS"));
			} catch (CommandFailedException e) {
				LOG.log(Level.WARNING,"Task " + id + " failed",  e);
				throw new RuntimeException("Task " + id + " failed",  e);
			}
			LOG.fine("Task " + id + " end");
		}	
	}
}
