/**
 * 
 */
package nl.infodation.wildflywrapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import nl.infodation.wildflywrapper.commands.WfCommand;
import nl.infodation.wildflywrapper.commands.WfDataSourceCmd;
import nl.infodation.wildflywrapper.commands.WfDeployCmd;
import nl.infodation.wildflywrapper.commands.WfErrorCode;
import nl.infodation.wildflywrapper.commands.WfSubSystem;
import nl.infodation.wildflywrapper.commands.WfUndeployCmd;

/**
 * @author hugo
 *
 */
public class WfServerTest {

	private static final Logger LOG = Logger.getLogger(WfServerTest.class.getSimpleName());

	private WfServer wf;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		wf = new WfServer("motorfiets");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		wf = null;
	}
	
	
	@Test
	public void testDeploy() throws Exception {
		File warFile= new File("c:\\Users\\hugo\\git\\mannetje - Copy (2)\\target\\ef.mannetje.war");
		assertTrue("Cannot read archive at " + warFile.getPath(),  warFile.canRead());
		wf.execute(WfDeployCmd.deploy(warFile, true));
		wf.execute(WfUndeployCmd.undeploy(warFile.getName()));
	}

	@Test
	public void testDeploy2() throws Exception {
		File warFile= new File("c:\\Users\\hugo\\git\\mannetje - Copy (2)\\target\\ef.mannetje.war");
		assertTrue("Cannot read archive at " + warFile.getPath(),  warFile.canRead());
		wf.execute(WfDeployCmd.deploy(warFile, true));
		try {
			wf.execute(WfDeployCmd.deploy(warFile, false));
			throw new Exception("Cannot deploy same deployment twice without force");
		} catch (CommandFailedException e) { // correct
			assertFalse(e.isRetryAble());
		}
		wf.execute(WfUndeployCmd.undeploy(warFile.getName()));
		try {
			wf.execute(WfUndeployCmd.undeploy(warFile.getName()));
			throw new Exception("Cannot undeploy same deployment twice");
		} catch (CommandFailedException e) { // correct
			assertEquals(WfErrorCode.WFLYCTL0062.name(), e.getErrorCode());
		}
	}

	@Test
	public void testDsExists() throws Exception {
		WfResponse result = wf.existsDs("ExampleDS");
		LOG.info("testDsExists(\"ExampleDS\"): " + result);
		assertTrue("true".equals(result.getServerMsg()));
	}

	@Test
	public void testDsExists2() throws Exception {
		try {
			WfResponse result = wf.existsDs("DoesNotExistDs");
			throw new Exception("Command should fail: " + result);
		} catch (CommandFailedException e) { // correct
			assertEquals(WfErrorCode.WFLYCTL0216.name(), e.getErrorCode());
		}
	}
	
	@Test
	public void testDriverExists() throws Exception {
		WfResponse result = wf.existsDriver("h2");
		LOG.info("testDsExists(\"h2\"): " + result);
		assertTrue("h2".equals(result.getServerMsg()));
	}

	@Test
	public void testDriverExists2() throws Exception {
		try {
			WfResponse result = wf.existsDriver("DoesNotExistDriverName");
			throw new Exception("Command should fail: " + result);
		} catch (CommandFailedException e) { // correct
			assertEquals(WfErrorCode.WFLYCTL0216.name(), e.getErrorCode());
		}
	}

	@Test
	public void testList() throws Exception {
		Collection<String> names = wf.list(WfSubSystem.datasources);
		assertNotNull(names);
		LOG.info(WfSubSystem.datasources + ": " + Arrays.toString(names.toArray()));
		assertTrue("Missing ExampleDS in " + names, names.contains("ExampleDS"));
		names = wf.list(WfSubSystem.logging);
		assertNotNull(names);
		LOG.info(WfSubSystem.logging + ": " + Arrays.toString(names.toArray()));
	}

	/**
	 * Test method for
	 * {@link nl.infodation.wildflywrapper.WfServer#execute(nl.infodation.wildflywrapper.commands.WfCommand)}.
	 */
	@Test
	public void testExecute() throws Exception {
		String testName = "HugoTest";
		try {
			WfCommand addDsCmd = WfDataSourceCmd.addDs("jdbc:mysql://localhost/" + testName, "admin", "admin");
			WfResponse result = wf.execute(addDsCmd);
			LOG.info("addDsCmd: " + result);
			assertNull(result.getLocalMsg());
			assertNotNull(result.getServerMsg());
			assertFalse("Reload not needed expected", result.isReloadNeeded());
			try {
				WfCommand testDsCmd = WfDataSourceCmd.testDs(testName);
				result = wf.execute(testDsCmd);
				LOG.info("testDsCmd: " + result);
				assertNull(result.getLocalMsg());
				assertNotNull(result.getServerMsg());
				assertFalse("Reload not needed expected", result.isReloadNeeded());
			} catch (CommandFailedException e) {
				throw e;
			}
		} catch (CommandFailedException e) {
			if (!WfErrorCode.WFLYCTL0158.name().equals(e.getErrorCode())) {
				throw e;
			}
			LOG.info("Unable to create datasource " + testName + " because it already exists");
		} finally {
			WfCommand removeDsCmd = WfDataSourceCmd.removeDs(testName);
			try {
				WfResponse result = wf.execute(removeDsCmd);
				LOG.info("removeDsCmd: " + result);
				assertEquals("{\"address\" => [(\"subsystem\" => \"datasources\"),(\"data-source\" => \"HugoTest\")],\"operation\" => \"remove\"}", result.getLocalMsg());
				assertNotNull(result.getServerMsg());
				assertTrue("Reload needed expected", result.isReloadNeeded());
			} catch (CommandFailedException e) {
				if ("WFLYCTL0216".equals(e.getErrorCode())) { // datasource not
																// found
					LOG.info("Unable to renove datasource " + testName + " because it could not be found");
				} else {
					throw e;
				}
			}
		}
	}

	/**
	 * Test method for
	 * {@link nl.infodation.wildflywrapper.WfServer#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		WfServer wf2 = new WfServer("password");
		assertEquals("If only the password is different, the two servers should be the same", wf, wf2);
	}
}
