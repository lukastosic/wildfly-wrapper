# Wildfly Wrapper

## Introduction

Wildfly wrapper is used as maven dependency.

Common use of this dependency is in projects that will be deployed to Wildfly/Jboss.

Using this dependency you can create datasource connections (or check for existing ones) on target wildfly/jboss server.

## Common use

Create `Main` method in your project (it sounds weird to have it in `war` application - but keep on). 

After `war` file is created you can execute `war` file with standard java command to execute `Main` method.

This `Main` method can call `Wildfly wrapper` to check for existing database connection and if necessary create new one.

## What actions are supported

Using `Wildfly wrapper` you can perform these actions:

### Data source actions

* check for existing datasource name
* check for existing database connection driver
* create new database connection
* delete existing database connection

### Deployment actions

* undeploy existing application
* deploy new application

### Custom actions

Alternatively you can execute any custom `jboss cli` supported command:
<url>https://docs.jboss.org/author/display/AS71/CLI+Recipes</url>


## Usage

### Add dependency

`Wildfly wrapper` is published to internal InfoDation _Artifactory_ server, so you can incorporate it in your project by adding dependency in `pom.xml` file:

```xml
<dependency>
    <groupId>nl.infodation</groupId>
    <artifactId>wildflywrapper</artifactId>
    <version>1.0.0</version>
</dependency>
```

In addition to dependency definition, you must also include _Artifactory_ repository information:

```xml
<repository>
    <snapshots>
        <enabled>false</enabled>
    </snapshots>
    <id>central</id>
    <name>libs-release</name>
    <url>http://10.4.1.218:8091/artifactory/libs-release</url>
</repository>
```

Note that this IP address is only available from InfoDation private network.

### Response object - ActionResult

Most of results are of type `WfResult`. This result is made of four fields:

* `localMsg` - (string) if result is successful it will show "user friendly" message, if not it will contain exception message
* `serverMsg` - (string) it contains wildfly response after executing `cli` command on server
* `reloadNeeded` - (boolean) if the command only will have effect after a reload of the config by the sevrer, this will be set to `true`. Typically true when removing datasource from wildfly server. 

### General init

You need to perform several steps:

Initalize `WfServer`:
WfServer can have several parameters. nearly all have default values, except for the password parameter.
Therefore you always start with a password:
```java
// "password" is password of Wildfly management account - given as string
WfServer wildfly = new WfServer("password");
// default value for port is 9990
wildfly.setPort(9990);
// default value for protocol is http+remote
wildfly.setProtocol("http+remote");
// default value for server is localhost
wildfly.setServer("localhost");
// default value for user is admin
wildfly.setUser("admin");
```

### Data Source actions

Once you have the WfServer initialized you can now execute commands using the `execute` method.
The execute method takes a wFCommand parameter. There are now few builders for those commands:
* WfDataqsourceCmd for commands about datasources 
* WfDeployCmd for several types of deployment commands
* wfUnDeployCmd for creating an undeploy command. 
* WfReadCmd for reading data from the wildfly server configuration and runtime.

#### for example: 
```java
wf.execute(WfDeployCmd.deploy(warFile, true));
```
will deploy the given warfile, even if there is laready a deployment with the same name (force = true) 

#### Check for existing datasource name

```java
WfResponse result = wf.existsDs("ExampleDS");
"true".equals(result.getServerMsg()); 
// true - data source with that name already exist
// false - data source with that name doesn't exist
```

### Custom `cli` actions


## Running unit tests

Running unit tests is a bit tricky because it requires existing WildFly instance to run commands on.

That is why you must adjust variables in the `@Before` part of unit tests (and also internal variables) to point to your running WildFly instance.

## Code is made using

* Eclipse (Neon)
* JUnit 4.12 (unit testing)
* SureFire (test results collection)
